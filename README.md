# README #

This README describes in summary what the Braintree SDK for Codeigniter is.

### What is the Braintree SDK For Codeigniter? ###

* This library is a wrapper around the Braintree PHP SDK library by Braintree.  This provides the minimal requirements of running the library in Codeigniter.
* Version 0.10

### How do I get set up? ###

* Download the code.
* Place the code in your applications folder.
* This requires the Codeigniter framework to be present

### Contribution guidelines ###

* To do

### Who do I talk to? ###

* You can contact me, Clint Christopher Canada at clint at ostalks dot com